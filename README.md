# Code Conformers Package

This package gives you tools to handle, amend and validate codes in any arbitrary format through any number of arbitrary manipulations. 

This package was developed for Laravel 8.0 and it is presented to you with an example implementation aimed at validating and amending South African cell numbers, but you could configure this package to conform, amend, validate any sort of UUID, token, alphanumerical code.

## Getting started

This implementation has NOT been packaged for distribution via composer as a standalone module, sorry!

This means that you will have to pull the whole Laravel application. Once you have deployed it locally, you will need to run: `composer install`in order to pull down all vendors' dependencies and the Laravel framework itself.

More info about how to install Laravel can be found [here](https://laravel.com/docs/8.x/installation). The whole process shouldn't take you more than a few minutes.

This Code COnformer package was developed and tested on Laravel 8.0 on a [Homestead](https://laravel.com/docs/8.x/homestead) Ubuntu virtual machine running PHP7.3 (No DB or webserver is required to run the tests or play with this example implementation)

## Basic usage


`$code = new SouthAfricanCell(1234, '27831234567'); // id, cell number`<br>
`$conformer = new CellNumbersConformer();`<br>
`$conformer->process($code);`<br>
`echo $code->isValid(); // will output 1`<br>
`echo empty($code->getLogs()); // will output 1`<br>

At the end of the conformation chain:

- a given code is and always was valid, if it's valid now and its log is empty
- a given code was not valid but was amended, if it's valid now but its log is not empty
- a given code was not valid and could not be amended, if it's still not valid and its log is not empty

(PLEASE NOTE: by default a CodeTansporter object is not valid upon instantiation and its log is empty. Further details about the validity of a code and its attached log will be given to you below in the following paragraphs)

## Code structure

Inside App/CodeConformers folder you will find 3 sub-folders:

- Conformers
- Manipulators
- Transporters

Those three folders contain the 3 interfaces (and related implementations) on which the package is built:

- `TransporterInterface`
- `ManipulatorIterface`
- `ConformerInterface`

A few more key elements are spread around the application:

- `App\Providers\CodeConformersProvider` provides binding of the implementations inside the Service Container
- several config files can be found inside config/codeConformers folder
- abundant testing has been provided for this example implementation based on `CodeConformersTestInterface` and `SouthAfricanTestAbstract` abstract class which will help you ensure that all elements of the manipulation chain would be tested against one same set of shared case studies (you can run the tests via the command: `php artisan test)
- a fake simple model `App\Models\SouthAfricanCell` which is not really but could easily be an ActiveRecord model 

PLEASE NOTE: all default classes have been bound as singleton and as deferrable to increase performance. This is OK because they don't need to hold a status and because we deal with one sole type of codes. Would you be dealing with different types of codes, you may need to change the binding accordingly

### Transporters

A transporter object implementing the `TransporterInterface` is just a wrapper for a code in any arbitrary format. The wrapper, however, also provides some features (like a validity flag and the ability of keeping a log of all manipulations performed on the wrappaed code) that will be used by the manipulators and the conformers. 

For this interface the package provides a `TransporterTrait` with a basic implementation that can be shared across multiple different types of codes and models. With minimal changes this trait could also be adapted to work with Eloquent models or other types of ActiveRecord models.

### Manipulators

Each object implementing the `ManipulatorIterface` is meant to perform one single and specific action on a transported code. This action may be a manipulation of the code itself or a validation of the code. When it comes to actual manipulation, each manipulator only intervenes when it can recognize inside the code a specific flaw that it knows how to fix (for instance: `SeparatorManipulator` will only intervene if it spots the presence of non digit chars inside the cell phone number).

A manipulator will add to the code's log only and only if one of these two conditions is true:

- the manipulator had effectively changed the code
- the code was deemed as not valid by a validating manipulator

Viceversa:

- the fact that one flaw was succesfully detected and corrected, doesn't mean that the code is now valid
- Neither the log or the validity flag will be touched if the given manipulator doesn't act. 
- the validity flag will be turned to true by a validation manipulator that deems the code as valid. 


### Conformers

Conformer objects are meant to hide the complexity of the manipulation chain (note that both interfaces declare the same method `process()`). The sequence of manipulators that each conformer will run on the codes can be controlled via config file. If you work with more than one type of codes, you should have more than one conformer (you can easily create more conformers by extending the `ConformerAbstract` class).

PLEASE NOTE:

- different conformers may share one or more manipulators
- the order of manipulators in the chain is RELEVANT and they will be executed in the same order as they are listed in the config file. In case of our South African cell numbers example, the DeletedManipulator is meant to always be executed for first.
- validation manipulators should always be the very last ones in the chain



## License

This code was developed by David Ballerini and belogns to him. If you like my work, hire me!
