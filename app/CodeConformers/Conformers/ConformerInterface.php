<?php


namespace App\CodeConformers\Conformers;


use App\CodeConformers\Manipulators\ManipulatorInterface;

/**
 * Interface ConformerInterface
 * @package App\CodeConformers\Conformers
 *
 * Notice that this interface adds nothing to the ManipulatorInterface.
 * From this point of view, you may think of the conformers as a sort of decorator of the manipulators.
 * This class was added so that you may be able to tell apart Conformers from Manipulators
 * without being forced to produce different / separated tests
 */
interface ConformerInterface
extends ManipulatorInterface
{
    //
    // nothing to add at the moment
    //
}
