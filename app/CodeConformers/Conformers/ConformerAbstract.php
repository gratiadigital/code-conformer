<?php


namespace App\CodeConformers\Conformers;


use App\CodeConformers\Transporters\TransporterInterface;
use Illuminate\Support\Facades\App;

class ConformerAbstract
implements ConformerInterface
{
    //
    // this constant MUST be overridden by children classes
    //
    public const CONF_FILE = '';

    //
    // this array will be populated with class names of Manipulators (order is RELEVANT)
    //
    protected $manipulators = [];


    public function __construct()
    {
        if
        (
            static::CONF_FILE == '' ||
            empty($this->manipulators = config(static::CONF_FILE, []))
        )
        {
            throw new \Exception('missing configurations ' . static::CONF_FILE);
        }
    }

    /**
     * This method acts as a sort of container / decorator to all other class::process() methods
     * of all Manipulators listed in the config.
     *
     * PLEASE NOTE: the order of the Manipulators is relevant
     *
     * @param TransporterInterface  $code   a code transporter of some kind
     */
    function process(TransporterInterface &$code)
    {
        foreach ($this->manipulators as $manipulatorClass)
        {
            //
            // retrieves singleton of the given Manipulator from the Service Container
            //
            $manipulator = App::make($manipulatorClass);
            $manipulator->process($code);

        }
    }
}
