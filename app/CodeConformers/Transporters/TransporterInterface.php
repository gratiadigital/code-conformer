<?php


namespace App\CodeConformers\Transporters;

/**
 * Interface TransporterInterface
 * @package App\CodeConformers
 * @subpackage App\CodeConformers\Transporters
 */
interface TransporterInterface
{
    /**
     * Getter method for the transported code
     *
     * @return String the alphanumerical code
     *
    */
    function getCode(): string;


    /**
     * Setter method for the transported code
     *
     * @param String $code
     */
    function setCode(string $code);


    /**
     * Checks if the transported code is allegedly valid
     *
     * @return bool
     */
    function isValid(): bool;

    /**
     * Sets a flag if the transported code is supposed to be valid
     * (the flag should be off by default until the code is proved valid)
     */
    function makeValid();

    /**
     * Sets the falg if the transported code is supposed to be invalid
     */
    function makeInvalid();

    /**
     * After manipulating the transported code, this method can be used to add notes to the log
     * regarding whatever manipulation has been performed
     *
     * @param String $log
     */
    function addLog(string $log);

    /**
     * Returns all the added notes as an array that could easily be imploded as a user friendly string
     *
     * @return array of the loaded comments
     */
    function getLogs(): array;

    /**
     * Resets the object to the default values
     */
    function flush();
}
