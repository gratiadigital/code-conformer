<?php


namespace App\CodeConformers\Transporters;

/**
 * Trait TransporterTrait
 * @package App\CodeConformers
 * @subpackage App\CodeConformers\Transporters
 *
 * This trait provides default implementations for the methods of the TransporterInterface
 * This implementation could easily be adapted for working with Eloquent models / Active Record models
 */
trait TransporterTrait
{
    //
    // code being transported
    //
    protected $code;

    //
    // validity bool flag
    //
    protected $valid = false;

    //
    // log of manipulations
    //
    protected $log = [];







    /**
     * @inheritDoc
     */
    function getCode(): string
    {
        return $this->code;
    }

    /**
     * @inheritDoc
     */
    function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @inheritDoc
     */
    function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @inheritDoc
     */
    function makeValid()
    {
        $this->valid = true;
    }

    /**
     * @inheritDoc
     */
    function makeInvalid()
    {
        $this->valid = false;
    }

    /**
     * @inheritDoc
     */
    function addLog(string $log)
    {
        $this->log[] = $log;
    }

    /**
     * @inheritDoc
     */
    function getLogs(): array
    {
        return $this->log;
    }

    /**
     * @inheritDoc
     */
    function flush()
    {
        $this->code = '';
        $this->valid = false;
        $this->log = [];
    }
}
