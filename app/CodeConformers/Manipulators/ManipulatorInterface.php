<?php


namespace App\CodeConformers\Manipulators;


use App\CodeConformers\Transporters\TransporterInterface;

/**
 * Interface ManipulatorInterface
 * @package App\CodeConformers
 * @subpackage App\CodeConformers\Manipulators
 *
 * THis interface describes classes that are meant to manipulate or to verify codes that have been wrapped
 * in CodeTransporter
 */
interface ManipulatorInterface
{

    /**
     * @param TransporterInterface $code    a code that has been properly wrapped for manipulation and transportation
     */
    function process(TransporterInterface &$code);
}
