<?php


namespace App\CodeConformers\Manipulators;


use App\CodeConformers\Transporters\TransporterInterface;

/**
 * Class DeletedManipulator
 * @package App\CodeConformers\Manipulators
 *
 * This Manipulator will alter the code if and only if the code contains the sub string '_DELETED_'.
 * In such case, the code string will be exploded and only the leftmost part will be kept.
 * That part can be either empty or not empty, depending on whether it was just a deletion
 * or a replacement with a new code.
 * The algorithm works even if the code has been deleted more than once.
 */
class DeletedManipulator
implements ManipulatorInterface
{

    protected $deletedTag;

    /**
     * @inheritDoc
     */
    function process(TransporterInterface &$code)
    {

        $toBeManipulated = $code->getCode();
        if(!is_string($toBeManipulated))
        {
            return;
        }

        //
        // this number has been deleted and perhaps replaced
        //
        $tag = $this->deletedTag;
        if(strpos($toBeManipulated, $tag) !== false)
        {
            //
            // keeps the leftmost part of the string which could be empty
            //
            $toBeManipulated = explode($tag, $toBeManipulated)[0];
            $code->addLog('This code was deleted');
            $code->setCode($toBeManipulated);
        }
    }

    public function __construct()
    {
        //
        // retrieves the deletion tag string from config or uses default
        //
        $this->deletedTag = config('codeConformers.manipulators.deleted', '_DELETED_');
    }
}
