<?php


namespace App\CodeConformers\Manipulators;


use App\CodeConformers\Transporters\TransporterInterface;


/**
 * Class ScientificNotationManipulator
 * @package App\CodeConformers\Manipulators
 *
 * This manipulator will intercept any code in scientific notation x.yEz
 * and will transform it into a common integer
 */
class ScientificNotationManipulator
implements ManipulatorInterface
{

    protected $regex;

    /**
     * @inheritDoc
     */
    function process(TransporterInterface &$code)
    {
        $toBeManipulated = $code->getCode();
        if(!is_string($toBeManipulated))
        {
            return;
        }

        //
        // uses regex to check if the code is expressed as a scientific notation string
        //
        if
        (
            preg_match($this->regex, $toBeManipulated)
        )
        {
            $code->setCode((string)intval($toBeManipulated));
            $code->addLog('scientific notation was transformed to integer');
        }

    }


    public function __construct()
    {
        //
        // retrieves the deletion tag string from config or uses default
        //
        $this->regex = config('codeConformers.manipulators.scientific', '/^[0-9.,]+E(\+|-)?\d+$/i');
    }
}
