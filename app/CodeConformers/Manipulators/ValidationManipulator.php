<?php


namespace App\CodeConformers\Manipulators;


use App\CodeConformers\Transporters\TransporterInterface;
use Illuminate\Support\Facades\Validator;

class ValidationManipulator
implements ManipulatorInterface
{
    //
    // validation rules
    //
    protected $rules = [];

    /**
     * @inheritDoc
     */
    function process(TransporterInterface &$code)
    {

        $code->makeInvalid(); // redundant but safe

        //
        // very first and basic validation of the code
        //
        $toBeValidated = $code->getCode();
        if(!is_string($toBeValidated))
        {
            $code->addLog('Wrong data type');
            return;
        }

        //
        // uses Laravel form validator to validate the code
        //
        $validator = Validator::make(
            ['code' => $toBeValidated],
            $this->rules
        );

        if($validator->fails())
        {
            //
            // glues together all possible errors as this is the feedback from one single module
            //
            $code->addLog(
                implode(',', $validator->errors()->all())
            );
            return;
        }

        $code->makeValid();
    }


    public function __construct()
    {
        //
        // retrieves validation rules from config
        //
        $this->rules = config('codeConformers.manipulators.validation', ['code' => 'required|numeric|digits:11|starts_with:2783']);
    }
}
