<?php


namespace App\CodeConformers\Manipulators;


use App\CodeConformers\Transporters\TransporterInterface;

/**
 * Class SeparatorManipulator
 * @package App\CodeConformers\Manipulators
 *
 * This manipulator will intercept and eliminate any non digit char in the code
 * (like for instance all common separators used to isolate the prefix: (), -, /)
 */
class SeparatorManipulator
implements ManipulatorInterface
{

    protected $regex;

    /**
     * @inheritDoc
     */
    function process(TransporterInterface &$code)
    {
        $toBeManipulated = $code->getCode();
        if(!is_string($toBeManipulated))
        {
            return;
        }

        //
        // uses regex to replace non digit chars with empty string
        //
        $modified = 0;
        $toBeManipulated = preg_replace($this->regex, '', $toBeManipulated, -1, $modified);
        if($modified)
        {
            $code->setCode($toBeManipulated);
            $code->addLog('separator chars were removed from the code');
        }

    }


    public function __construct()
    {
        //
        // retrieves the deletion tag string from config or uses default
        //
        $this->regex = config('codeConformers.manipulators.separator', '/\D/');
    }
}
