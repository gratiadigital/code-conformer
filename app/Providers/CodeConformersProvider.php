<?php

namespace App\Providers;

use App\CodeConformers\Conformers\CellNumbersConformer;
use App\CodeConformers\Manipulators\DeletedManipulator;
use App\CodeConformers\Manipulators\ScientificNotationManipulator;
use App\CodeConformers\Manipulators\SeparatorManipulator;
use App\CodeConformers\Manipulators\ValidationManipulator;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

/**
 * Class CodeConformersProvider
 * @package App\CodeTransformers
 *
 * This service provider registers all the classes which the CodeTransformer package is based on.
 * Since none of these classes retain a state (except for the their configs) all can be implemented
 * as singletons to improve performances. For the same reason, this provider is deferrable
 * (all classes will be only loaded once, if and when they will be needed)
 */
class CodeConformersProvider
extends ServiceProvider
implements DeferrableProvider
{

    public $singletons = [
        //
        // manipulators
        //
        DeletedManipulator::class => DeletedManipulator::class,
        ScientificNotationManipulator::class => ScientificNotationManipulator::class,
        SeparatorManipulator::class => SeparatorManipulator::class,
        ValidationManipulator::class => ValidationManipulator::class,

        //
        // conformers
        //
        CellNumbersConformer::class => CellNumbersConformer::class,
    ];


    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // all singleton bindings are provided by the $singletons property
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return array_values($this->singletons);
    }
}
