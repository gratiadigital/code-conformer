<?php


namespace App\Models;


use App\CodeConformers\Transporters\TransporterInterface;
use App\CodeConformers\Transporters\TransporterTrait;


/**
 * Class SouthAfricanCell
 * @package App\Models
 *
 * This class is just a simple example of implementation for the CodeTransformers package.
 * This is not an Eloquent / Active Record model but the example could easily be adapted to work with DB records
 */
class SouthAfricanCell
implements TransporterInterface
{

    //
    // if this model were an Active Record / Eloquent model this id could be read from DB
    //
    public $id;


    /**
     * This trait provides default implementation for the methods of the TransporterInterface
     * The trait brings in the following properties:
     *
     * @property string $code
     * @property bool   $valid
     * @property array  $log
     */
    use TransporterTrait;


    public function __construct(int $id, string $code, bool $valid = false)
    {

        $this->id = $id;
        $this->code = $code;
        $this->valid = $valid;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }



}
