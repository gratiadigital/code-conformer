<?php

use \App\CodeConformers\Manipulators\DeletedManipulator;
use App\CodeConformers\Manipulators\ScientificNotationManipulator;
use App\CodeConformers\Manipulators\SeparatorManipulator;
use App\CodeConformers\Manipulators\ValidationManipulator;


/**
 * This config file provides the sequence of Manipulators that may be used on a CodeTransporter object
 * transporting a South African Cellphone number
 *
 * PLEASE NOTE: the ordering of this array determines the order of execution and it's RELEVANT
 */
return [
    DeletedManipulator::class,
    ScientificNotationManipulator::class,
    SeparatorManipulator::class,
    ValidationManipulator::class,
];
