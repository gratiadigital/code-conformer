<?php

return [

    //
    // validation rules that can be used by a Laravel validator instance
    //
    'code' => 'required|numeric|digits:11|starts_with:2783',
];
