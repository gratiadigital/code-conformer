<?php


namespace Tests;

/**
 * Interface CodeConformersTestInterface
 * @package Tests
 *
 * This interface rules how all tests reltaed to the CodeTransformers package may share one same set
 * of case studies for their tests.
 * @see SouthAfricanTestAbstract for default implementation
 */
interface CodeConformersTestInterface
{
    /**
     * This method can provide data for all unit tests related to the South African example.
     * Initial data is the same for every test but not so the expected result,
     * hence the expected results are being mixed in by the test class itself
     *
     * @param   array   $expectedResults    the test class will provide the expected results through this array
     * @return  array
     */
    public function southAfricanNumbersProvider(): array;
}
