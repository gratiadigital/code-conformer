<?php

namespace Tests\Feature;

use App\CodeConformers\Conformers\CellNumbersConformer;
use Tests\CodeConformersTestInterface;
use Tests\SouthAfricanTestAbstract;

class CellNumberConformerTest
extends SouthAfricanTestAbstract
implements CodeConformersTestInterface
{

    protected $expectedResults = [

        // as per instructions
        'correct code' => [
            'hasLog' => false,
            'isValid' => true,
            'code' => '27831234567',
        ],

        // very similar number but short
        'short code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '2783123456',
        ],

        // very similar number but short
        'wrong prefix' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '2782123456',
        ],

        // it shall pass as it can be casted to string by PHP
        'wrong data type' => [
            'hasLog' => false,
            'isValid' => true,
            'code' => '27831234567',
        ],

        //taken from the exercise original list file
        'deleted' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '',
        ],

        'scientific notation amendable' => [
            'hasLog' => true,
            'isValid' => true,
            'code' => '27831234567',
        ],
        'scientific notation non amendable' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '278312',
        ],
        'with separator' => [
            'hasLog' => true,
            'isValid' => true,
            'code' => '27831234567',
        ],
        'deleted and replaced with correct code' => [
            'hasLog' => true,
            'isValid' => true,
            'code' => '27836826107',
        ],
        'deleted many times and replaced with correct code' => [
            'hasLog' => true,
            'isValid' => true,
            'code' => '27836826107',
        ],
        'deleted and replaced with amendable code' => [
            'hasLog' => true,
            'isValid' => true,
            'code' => '27831234567',
        ],
        'deleted and replaced with wrong code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '639156553262',

        ],

    ];


    /**
     * Unit test for the ScientificNotationManipulatorTest.
     *
     * @test
     * @dataProvider southAfricanNumbersProvider
     * @param array $original   [id, code] as necessary to instantiate a CodeTransporter object
     * @param array $result     expected results from the test
     * @return void
     * @throws \Exception
     */
    public function testSeparatorManipulator(array $original, array $result)
    {
        //
        // instantiates the specific manipulator that is to be tested
        //
        $manipulator = new CellNumbersConformer();

        //
        // performs the actual testing
        //
        $this->testManipulator($original, $result, $manipulator);

    }
}
