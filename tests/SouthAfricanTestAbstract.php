<?php


namespace Tests;


use App\CodeConformers\Manipulators\ManipulatorInterface;
use App\Models\SouthAfricanCell;

/**
 * Class SouthAfricanTestAbstract
 * @package App\CodeConformers
 *
 * This class provides some shared / backbone implementations to all tests related to the CodeManipulators package
 * Orginal data is
 */
abstract class SouthAfricanTestAbstract
extends TestCase
{
    //
    // provides original case studies for all the tests
    //(starting data should be the same for every final class)
    //
    private $caseStudies = [

        // the example cell number from the exercise
        'correct code' => [
            'id' => 1,
            'code' => '27831234567',
         ],

        // very similar but one digit short
        'short code' => [
            'id' => 2,
            'code' => '2783123456',
        ],

        // very similar but one digit is wrong in the prefix
        'wrong prefix' => [
            'id' => 2,
            'code' => '2782123456',
        ],

        // integer code instead of string
        'wrong data type' => [
            'id' => 3,
            'code' => 27831234567,
        ],

        //taken from the exercise original list file
        'deleted' => [
            'id' => 103231021,
            'code' => '_DELETED_1486284887',
        ],

        //
        // in the exercise Excel file i found some numbers that are displayed in scientific notation
        // honestly it seems odd (perhaps a quirk added by Excel) especially considering that are all wrong numbers
        // but I'm playing along for the ske of the argument
        //
        'scientific notation amendable' => [
            'id' => 4,
            'code' => '2.7831234567E+10',
        ],
        'scientific notation non amendable' => [
            'id' => 5,
            'code' => '2.7831234567E+5',
        ],
        'with separator' => [
            'id' => 3,
            'code' => '2783-1234567',
        ],
        'deleted and replaced with correct code' => [
            'id' => 103427420,
            'code' => '27836826107_DELETED_1488996550',
        ],
        'deleted many times and replaced with correct code' => [
            'id' => 103427420,
            'code' => '27836826107_DELETED_1488996550_DELETED_999',
        ],
        'deleted and replaced with amendable code' => [
            'id' => 3,
            'code' => '2.7831234567E+10_DELETED_1488996550',
        ],

        // taken from the exercise csv file
        'deleted and replaced with wrong code' => [
            'id' => 103266195,
            'code' => '639156553262_DELETED_1486721886',
        ],
    ];

    //
    // this property should be overridden by children classes
    // as each manipulator will provide different results
    //
    protected $expectedResults = [];




    /**
     * This method can provide data for all unit tests related to the South African example.
     * Initial data is the same for every test but not so the expected result,
     * hence the expected results are being mixed in by the test class itself
     *
     * @return  array
     */
    public function southAfricanNumbersProvider(): array
    {
        $cases = $this->caseStudies;
        $results = $this->expectedResults;

        //
        // mixes initial data with the expected results from the test.
        //
        $testData = [];
        foreach ($cases as $name => $initialValues)
        {
            switch (true)
            {
                case isset($results[$name]):
                    $testData[$name] = [$initialValues, $results[$name]];
                    break;

                default:
                    //Log::debug(__CLASS__ . ": Test data is incomplete for case: $name");
                    echo __CLASS__ . ": Test data is incomplete for case: $name\n";
            }

        }

        return $testData;
    }

    /**
     * Provides a backbone implementation to all the tests for all different Manipulator classes.
     *
     * @param array $original                   from the dataProvider
     * @param array $result                     from the dataProvider
     * @param ManipulatorInterface $manipulator a specific manipulator is injected by the the children's testMethod()
     */
    protected function testManipulator(array $original, array $result, ManipulatorInterface $manipulator)
    {
        $code = new SouthAfricanCell($original['id'], $original['code']);

        $manipulator->process($code);

        $this->assertTrue($result['isValid'] == $code->isValid());
        $this->assertTrue($result['code'] == $code->getCode());
        $this->assertTrue($result['hasLog'] == !empty($code->getLogs()));


    }



}
