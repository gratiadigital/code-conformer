<?php

namespace Tests\Unit;

use App\CodeConformers\Manipulators\DeletedManipulator;
use App\CodeConformers\Manipulators\ValidationManipulator;
use Tests\CodeConformersTestInterface;
use Tests\SouthAfricanTestAbstract;

class DeletedManipulatorTest
extends SouthAfricanTestAbstract
implements CodeConformersTestInterface
{
    protected $expectedResults = [

        // as per instructions
        'correct code' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '27831234567',
        ],

        // very similar number but short
        'short code' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '2783123456',
        ],

        // very similar number but short
        'wrong prefix' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '2782123456',
        ],

        // it shall pass as it can be casted to string by PHP
        'wrong data type' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => 27831234567,
        ],

        //taken from the exercise original list file
        'deleted' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '',
        ],

        'scientific notation amendable' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '2.7831234567E+10',
        ],
        'scientific notation non amendable' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '2.7831234567E+5',
        ],
        'with separator' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '2783-1234567',
        ],
        'deleted and replaced with correct code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '27836826107',
        ],
        'deleted many times and replaced with correct code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '27836826107',
        ],
        'deleted and replaced with amendable code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '2.7831234567E+10',
        ],
        'deleted and replaced with wrong code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '639156553262',

        ],

    ];



    /**
     * Unit test for the ValidationManipulator.
     *
     * @test
     * @dataProvider southAfricanNumbersProvider
     * @return void
     */
    public function testValidationManipulator(array $original, array $result)
    {
        //
        // instantiates the specific manipulator that is to be tested
        //
        $manipulator = new DeletedManipulator();

        //
        // performs the actual testing
        //
        $this->testManipulator($original, $result, $manipulator);

    }
}
