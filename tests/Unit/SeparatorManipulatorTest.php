<?php

namespace Tests\Unit;

use App\CodeConformers\Manipulators\SeparatorManipulator;
use Tests\CodeConformersTestInterface;
use Tests\SouthAfricanTestAbstract;

class SeparatorManipulatorTest
extends SouthAfricanTestAbstract
implements CodeConformersTestInterface
{

    //
    // The SeparatorManipulator is meant to be always run AFTER the DeletedManipulator
    // Hence some of these following test results are not really expected in practice
    // They would only occur if the DeletedManipulator had not been run before
    //
    protected $expectedResults = [

        // as per instructions
        'correct code' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '27831234567',
        ],

        // very similar number but short
        'short code' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '2783123456',
        ],

        // very similar number but short
        'wrong prefix' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '2782123456',
        ],

        // it shall pass as it can be casted to string by PHP
        'wrong data type' => [
            'hasLog' => false,
            'isValid' => false,
            'code' => '27831234567',
        ],

        //taken from the exercise original list file
        'deleted' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '1486284887',
        ],

        'scientific notation amendable' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '2783123456710',
        ],
        'scientific notation non amendable' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '278312345675',
        ],
        'with separator' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '27831234567',
        ],
        'deleted and replaced with correct code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '278368261071488996550',
        ],
        'deleted many times and replaced with correct code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '278368261071488996550999',
        ],
        'deleted and replaced with amendable code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '27831234567101488996550',
        ],
        'deleted and replaced with wrong code' => [
            'hasLog' => true,
            'isValid' => false,
            'code' => '6391565532621486721886',

        ],

    ];



    /**
     * Unit test for the ScientificNotationManipulatorTest.
     *
     * @test
     * @dataProvider southAfricanNumbersProvider
     * @return void
     */
    public function testSeparatorManipulator(array $original, array $result)
    {
        //
        // instantiates the specific manipulator that is to be tested
        //
        $manipulator = new SeparatorManipulator();

        //
        // performs the actual testing
        //
        $this->testManipulator($original, $result, $manipulator);

    }
}
