<?php

namespace Tests\Unit;

use App\CodeConformers\Manipulators\ValidationManipulator;
use Tests\CodeConformersTestInterface;
use Tests\SouthAfricanTestAbstract;

class ValidationManipulatorTest
extends SouthAfricanTestAbstract
implements CodeConformersTestInterface
{

    protected $expectedResults = [

            // as per instructions
            'correct code' => [
                'hasLog' => false,
                'isValid' => true,
                'code' => '27831234567',
            ],

            // very similar number but short
            'short code' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '2783123456',
            ],

            // very similar number but short
            'wrong prefix' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '2782123456',
            ],

            // it shall pass as it can be casted to string by PHP
            'wrong data type' => [
                'hasLog' => false,
                'isValid' => true,
                'code' => 27831234567,
            ],

            //taken from the exercise original list file
            'deleted' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '_DELETED_1486284887',
            ],

            'scientific notation amendable' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '2.7831234567E+10',
            ],
            'scientific notation non amendable' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '2.7831234567E+5',
            ],
            'with separator' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '2783-1234567',
            ],
            'deleted and replaced with correct code' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '27836826107_DELETED_1488996550',
            ],
            'deleted many times and replaced with correct code' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '27836826107_DELETED_1488996550_DELETED_999',
            ],
            'deleted and replaced with amendable code' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '2.7831234567E+10_DELETED_1488996550',
            ],
            'deleted and replaced with wrong code' => [
                'hasLog' => true,
                'isValid' => false,
                'code' => '639156553262_DELETED_1486721886',

            ],

        ];


    /**
     * Unit test for the ValidationManipulator.
     *
     * @test
     * @dataProvider southAfricanNumbersProvider
     * @return void
     */
    public function testValidationManipulator(array $original, array $result)
    {
        //
        // instantiates the specific manipulator that is to be tested
        //
        $manipulator = new ValidationManipulator();

        //
        // performs the actual testing
        //
        $this->testManipulator($original, $result, $manipulator);

    }
}
